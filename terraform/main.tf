# AWS S3 bucket creation
resource "aws_s3_bucket" "my_s3_bucket" {
  bucket = "my-s3-bucket-devops-internship"
}

# S3 Bucket Versioning enabled
resource "aws_s3_bucket_versioning" "s3_versioning" {
  bucket = aws_s3_bucket.my_s3_bucket.id
  versioning_configuration {
      status = "Enabled"
    }
}

# S3 Bucket Ownership Controls
resource "aws_s3_bucket_ownership_controls" "bucket_ownership" {
  bucket = aws_s3_bucket.my_s3_bucket.id
  rule {
          object_ownership = "BucketOwnerPreferred"
        }
}

# S3 Bucket Public Access Enable
resource "aws_s3_bucket_public_access_block" "public_access" {
  bucket = aws_s3_bucket.my_s3_bucket.id
  block_public_acls         = false
  block_public_policy       = false
  ignore_public_acls        = false
  restrict_public_buckets   = false
}

# S3 Bucket Access Control List Enable
resource "aws_s3_bucket_acl" "bucket_acl" {
  	  depends_on = [
    	    aws_s3_bucket_ownership_controls.bucket_ownership,
    	    aws_s3_bucket_public_access_block. public_access
  	  ]
  bucket = aws_s3_bucket.my_s3_bucket.id
  acl    = "public-read-write"
}

# --------------------------------- #

# AWS Security Group Creation for Firewall on EC2 instance in Mumbai region
resource "aws_security_group" "internship_sg" {
    name = "internship_sg"
    vpc_id = "vpc-0185dc2f12722a76c"
    ingress {  // for HTTP
       from_port = 80
       protocol  = "tcp"
       to_port   = 80
       cidr_blocks = ["0.0.0.0/0"]  
    }
    ingress {  // for SSH
       from_port = 22
       protocol  = "tcp"
       to_port   = 22
       cidr_blocks = ["0.0.0.0/0"]  
    }
    egress {
       from_port = 0
       protocol  = -1
       to_port   = 0
       cidr_blocks = ["0.0.0.0/0"]    
    }
    tags = {
       Name = "internship_sg"
   }
}

# AWS EC2 Instance creation with the above SG and AMI Ubuntu OS 24.04 in Mumbai region 
resource "aws_instance" "internship_ec2_instance" {
    ami = "ami-0f58b397bc5c1f2e8"
    key_name = "devops-internship"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.internship_sg.id]
    subnet_id = "subnet-049b4be03b08ee462"
    tags = {
       Name = "internship_ec2_instance"
    }
}






