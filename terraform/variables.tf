# Defining Region
variable "aws_region" {
  default = "ap-southeast-1"  // Singapore 
}

# Defining CIDR Block for VPC
# Showing demo
variable "vpc_cidr" {
  default = "10.0.0.0/16"  // 32-16 = 16 = 2^16 ec2 instances can be made accommodated in the VPC 
}
